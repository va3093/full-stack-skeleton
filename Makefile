VERSION=`cat VERSION`

db_migrate:
	FLASK_APP=app flask db migrate

db_init:
	FLASK_APP=app flask db init

db_upgrade:
	FLASK_APP=app flask db upgrade

build:
	docker build --no-cache -t registry.gitlab.com/va3093/meos_website:master .;

run:
	docker run -p 5001:8080 --name api --rm registry.gitlab.com/va3093/meos_website:master

deploy_image: build
	# You will first need to do `docker login registry.gitlab.com`
	docker push registry.gitlab.com/va3093/meos_website:master
	# This is optional
	$(ANSIBLE_DEPLOY_SCRIPT)

build_client_dev:
	cd app/client/vue_app; npm run build-dev

run_client_dev:
	cd app/client/vue_app; npm run serve

run_dev:
	FLASK_APP=app flask run

npm_install:
	cd app/client/vue_app; npm install

run_postgres:
	docker run  \
	--env POSTGRES_PASSWORD=password \
	--env POSTGRES_USER=app \
	--env POSTGRES_DB=app \
	--rm \
	--name postgres \
	-d \
	-p 5432:5432 \
	postgres:9.5

