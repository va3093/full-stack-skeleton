
import datetime
import json

from urllib import parse

from flask import request

from app.config import Config


def additional_json_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.isoformat()


def json_response(*, app, json_string=None, response_dict=None, status=200):
    response = app.response_class(
        response=json_string or json.dumps(
            response_dict, default=additional_json_encoder),
        status=status,
        mimetype='application/json'
    )
    return response


def add_header(response):
    response.headers[
        'Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    origin = request.headers.get('Origin')
    if origin:
        parsed_url = parse.urlparse(origin)
        if parsed_url.netloc.endswith(Config.MEOS_DOMAIN):
            response.headers[
                'Access-Control-Allow-Origin'] = f"https://{parsed_url.netloc}"
            response.headers['Access-Control-Allow-Credentials'] = 'true'
    # Required for Webpack dev application to make  requests to flask api
    # from another host (localhost:8080)
    if Config.ALLOW_ALL_CORS:
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers["Access-Control-Allow-Methods"] = (
            "GET,POST,HEAD,OPTIONS,PUT,DELETE,PATCH")
    return response
