import json
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import base64

from jinja2 import Template as JinjaTemplate

from app.config import Config


DEFAULT_SENDER = 'Meos <website@meos.app>'


class Template:

    def __init__(
            self, *, id, subject,
            msg_html, msg_plain):
        self.id = id
        self.subject = subject
        self.msg_html = msg_html
        self.msg_plain = msg_plain

    def generate_mime_multipart(
            self, *, sender=DEFAULT_SENDER, recipient):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.subject
        msg['From'] = sender
        msg['To'] = recipient
        msg.add_header('reply-to', sender)
        if self.msg_plain:
            msg.attach(MIMEText(self.msg_plain, 'plain'))
        if self.msg_html:
            msg.attach(MIMEText(self.msg_html, 'html'))
        raw = base64.urlsafe_b64encode(msg.as_bytes())
        raw = raw.decode()
        body = {'raw': raw}
        return body

    @classmethod
    def load_template(cls, name, **kwargs):
        path = generate_template_path(name)
        with open(path) as f:
            template = json.load(f)
        html = template.get('msg_html', "")
        if html:
            jinja_template = JinjaTemplate(html)
            html = jinja_template.render(host=Config.CLIENT_URL, **kwargs)
        return cls(
            id=name,
            subject=template.get('subject'),
            msg_html=html,
            msg_plain=template.get('msg_plain', "").format(**kwargs)
        )


def generate_template_path(file):
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, f'templates/{file}.json')
    return filename
