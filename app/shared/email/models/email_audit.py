from app.shared.database import (
    SurrogatePK, Model, Column, db, OutputMixin,
    Auditable)


class EmailAudit(SurrogatePK, Model, OutputMixin, Auditable):
    __tablename__ = 'email_audit'
    template_id = Column(
        db.String, unique=False, nullable=True, index=True)
