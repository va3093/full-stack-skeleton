import httplib2
import re
import os
import oauth2client
from apiclient import errors, discovery
import logging

from .templates import Template
from .models import EmailAudit

_logger = logging.getLogger(__name__)


class EmailManager:

    @staticmethod
    def test():
        raise Exception

    def __init__(self, *, credentials_path="", user):
        self.credentials_path = credentials_path
        self.user = user

    @property
    def credentials(self):
        try:
            return self.__credentials
        except AttributeError:
            pass
        if not os.path.exists(self.credentials_path):
            self.__credentials = None
        else:
            store = oauth2client.file.Storage(self.credentials_path)
            credentials = store.get()
            self.__credentials = credentials
        return self.__credentials

    def should_send_email(self, *, max_send_count, template_id):
        if not max_send_count:
            return True
        else:
            previous_emails = (
                EmailAudit.query
                .filter_by(user_id=self.user.id, template_id=template_id)
                .all())
            return len(previous_emails) < max_send_count

    def send_email(
            self, *, template_id, max_send_count=None, on_success=lambda: True,
            template_kwargs=None):
        if self.should_send_email(
                max_send_count=max_send_count, template_id=template_id):
            template_kwargs = template_kwargs or {}
            template = Template.load_template(template_id, **template_kwargs)
            response = self.send_email_with_template(
                template=template, recipient=self.user.email_address,
                on_success=on_success
            )
            if response:
                self.create_email_audit(template_id=template_id)
        else:
            _logger.info(
                f'Aborted sending email to {self._anonymous_email()} '
                'because should_send_email returned False')

    def _anonymous_email(self):
        return re.sub(r'.+(?=@.+\.(.+))', "xxxx",
                      self.user.email_address or "")

    def create_email_audit(self, *, template_id):
        EmailAudit.create(
            user_id=self.user.id,
            template_id=template_id
        )

    def send_email_with_template(
            self, *, template, recipient, on_success=lambda: True):
        if self.credentials is None:
            _logger.warn(
                "Tried to send email, but no credentials were"
                "found")
            return None
        http = self.credentials.authorize(httplib2.Http())
        service = discovery.build('gmail', 'v1', http=http,
                                  cache_discovery=False)
        message = template.generate_mime_multipart(recipient=recipient)
        user_id = 'me'
        try:
            message = (service.users().messages().send(
                userId=user_id, body=message).execute())
            _logger.info(
                f"Sent email ({message['id']}) with template "
                f"{template.id} to {self._anonymous_email()}")
            on_success()
            return message
        except errors.HttpError as error:
            _logger.warn('An error occurred when sending email: %s' % error)
