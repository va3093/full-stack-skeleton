import Vue from 'vue'
import Router from 'vue-router'
import constants from './constants.js'

const Home = () => import('./views/Home.vue') // eslint-disable-line
const PrivacyPolicy = () => import('./views/PrivacyPolicy.vue')

Vue.use(Router)

const defaultProps = {
  meosStyle: constants.meosStyle
}

export default new Router({
  mode: 'history',
  // scrollBehavior,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: defaultProps
    },
    {
      path: '/client/privacy_policy',
      name: 'privacyPolicy',
      component: PrivacyPolicy,
      props: defaultProps
    },
  ]
})
