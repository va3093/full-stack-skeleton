import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueMq from 'vue-mq'
import VueMultianalytics from 'vue-multianalytics'
import VueCookies from 'vue-cookies'
import 'vuejs-dialog/dist/vuejs-dialog.min.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

import i18n from './content'

import './filters'

Vue.use(VueAxios, axios)
Vue.use(VueCookies)
Vue.use(VueMq, {
  breakpoints: {
    'mobile small': 350,
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity
  }
})

let mixpanelConfig = {
  token: process.env.VUE_APP_MIXPANEL_TOKEN || 'a'
}

Vue.use(VueMultianalytics, {
  modules: {
    mixpanel: mixpanelConfig
  },
  routing: {
    vueRouter: router, //  Pass the router instance to automatically sync with router (optional)
    ignoredViews: [], // Views that will not be tracked
    ignoredModules: [] // Modules that will not send route change events. The event sent will be this.$ma.trackView({viewName: 'homepage'}, ['ga'])
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
