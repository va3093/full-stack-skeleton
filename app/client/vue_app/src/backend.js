import axios from 'axios'

const API_URL = process.env.VUE_APP_API_URL

let $axios = axios.create({
  baseURL: API_URL,
  timeout: 5000,
  headers: {'Content-Type': 'application/json'}
})

// Response Interceptor to handle and log errors
$axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  // Handle Error
  console.log(error)
  return Promise.reject(error)
})

export default {

  testFetch () {
    return $axios.get(`/api/example`, {})
      .then(response => response.data)
  }
}
