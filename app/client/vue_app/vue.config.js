'use strict'
var webpack = require('webpack')

module.exports = {
  configureWebpack: {
    // Merged into the final Webpack config
    plugins: [
      new webpack.DefinePlugin({})
    ]
  }
}
