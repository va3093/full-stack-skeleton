# Meos Privacy Policy

## Introduction

At Meos our vision is to put the user back in control of their data. Our first step is helping users understand what they can learn from their data.

This policy explains how we use your personal data. We want to help you understand how we work with your data, so that you can make informed choices and be in control of your information. We invite you to spend a few moments understanding this policy. We may update this policy from time to time and we will notify you when we do so.

This policy currently only covers our #getyourData service. When we update this policy to include more of our products, all users will be notified.

This policy covers:
- Who we are;
- What personal data we hold and how we get it;
- What we use your personal data for;
- Sharing your personal data;
- Retention;
- Data security and transfers; and
- Your rights.

## Who we are and how to contact us
We are a registered company under Meos Tech Ltd (11482890) at UK's Companies House. If you have any queries please send an email to privacy@meos.app.

## What personal data do we hold and how do we process it

### GetYourData
The #getyourdata product gathers the following data about you

**Email Address**: Your email address is gathered and stored so that we can contact you when your data archive is ready and in order for us to contact you when any events related to your account occur.

**Summary of you Data**: We collect the data stored about you from 3rd party web services like Google. We then process it to reveal trends and interesting information about your data (Eg. amount of mony spend on amazon). We store this data so that you can later retrieve it quickly.

**App Usage Analytics**: When you use our App, we may automatically collect information about how you are using the app. We collect this data so that we can understand how to change it to result in the best user experience for our users. This data may include things like the ip address used to connect your mobile phone or other device to the Internet, your login information, system and operating system type and version, browser or app version, time zone setting, operating system and platform, location, information about your visit, including products and services you viewed or used, App response times, interaction information (such as button presses).

We work with partners who provide us with analytics and advertising services (for our services only and not for third party advertising). This includes helping us understand how users interact with our services, providing our advertisements on the internet, and measuring performance of our services and our adverts. Cookies and similar technologies may be used to collect this information, such as your interactions with our services.

**Information obtained from third party services**: In order to process your data from Google we allow you to authorise us to access data from Google drive and Gmail. When you choose to do this we access your email address, your Google TakeOut archive and specific emails that we use to produce insights about your data. This data is stored for a short time to do the processing. As soon as the processing is done the data is deleted and only the summaries and aggregations remain.


## Sharing your personal data with others

- We may share your personal data with companies we have hired to provide services on our behalf, including those who act as data processors on our behalf, acting strictly under contract in accordance with Article 28 GDPR. Those data processors are bound by strict confidentiality and data security provisions, and they can only use your data in the ways specified by us.
- We may share with our commercial partners aggregated data that does not personally identify you, but which shows general trends, for example, the number of users of our service.
- We may preserve or disclose information about you to comply with a law, regulation, legal process, or governmental request; to assert legal rights or defend against legal claims; or to prevent, detect, or investigate illegal activity, fraud, abuse, violations of our terms, or threats to the security of our services or the physical safety of any person.

Except as described above, we will never share your personal information with any other party without your consent.

## Data storage, security and transfers
We encrypt data transmitted to and from the App. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.

Your data may be processed or stored via destinations outside the European Economic Area, but always in accordance with data protection law and subject to strict safeguards. For example, we work with third parties to use their software platforms who have servers outside the UK or EEA to send communication emails to our users.

## Your rights
As indicated above, whenever we rely on your consent to process your personal data, you have the right to withdraw your consent at any time by accessing the privacy settings in your account.

You also have specific rights under the GDPR and DPA to:

- Wherever we process data based on your consent, withdraw that consent at any time. You can do this via the privacy section of our Account;
- Understand and request a copy of information we hold about you.
- Ask us to rectify or erase information we hold about you
- Ask us to restrict our processing of your personal data or object to our processing; and
- Ask for your data to be provided on a portable basis.

You may also contact the Information Commissioners Office (the data protection regulator in the UK): Information Commissioner's Office, Wycliffe House, Water Lane, Wilmslow, Cheshire, SK9 5AF, telephone: 0303 123 1113 (local rate).