""" Client App """

from flask import Blueprint, render_template

client_bp = Blueprint('client_app', __name__,
                      url_prefix='',
                      static_url_path='',
                      static_folder='./vue_app/dist',
                      template_folder='./vue_app/dist',
                      )


@client_bp.route('/', defaults={'path': ''})
@client_bp.route('/client/<path:path>')
def index(path):
    return render_template('index.html')


@client_bp.route('google8e4af1879134c841.html')
def google_verification():
    return "google-site-verification: google8e4af1879134c841.html"
