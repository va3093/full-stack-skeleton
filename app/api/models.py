from app.shared.database import (
    SurrogatePK, Model, Column, db, Auditable, OutputMixin)


class Example(SurrogatePK, Model, Auditable, OutputMixin):
    __tablename__ = 'example'
    example_column = Column(db.String(200), nullable=False)
