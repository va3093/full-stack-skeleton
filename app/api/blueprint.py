""" API Blueprint Application """
import logging
from functools import partial

from flask import Blueprint, current_app

from app.config import Config
from app.shared.database import db
from app.shared.http import json_response
from .models import Example

api_bp = Blueprint('api_bp', __name__,
                   template_folder='templates',
                   url_prefix='/api')

json_response = partial(json_response, app=current_app)

_logger = logging.getLogger(__name__)


@api_bp.after_request
def add_header(response):
    response.headers[
        'Access-Control-Allow-Headers'] = 'Content-Type,Authorization'

    # Required for Webpack dev application to make  requests to flask api
    # from another host (localhost:8080)
    if Config.ALLOW_ALL_CORS:
        response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@api_bp.route('/health', methods=['GET'])
def health():
    db.session.execute(f'SELECT 1 FROM email')
    return 'Ok', 200


@api_bp.route('/version', methods=['GET'])
def get_version():
    with open('VERSION') as f:
        version = f.read()
    return version, 200


@api_bp.route('/example', methods=['GET'])
def example():
    example = Example(example_column='example').save()
    example_get = Example.get_by_id(example.id)
    return json_response(response_dict=example_get.to_dict())
