from flask import Flask

from app.api import api_bp  # noqa
from app.client import client_bp
from app.extensions import register_extensions
from app.config import Config
from app.logging import configure_logging


app = Flask(__name__)
app.config.from_object(Config)
app.secret_key = Config.SECRET_KEY
app.register_blueprint(api_bp)
app.register_blueprint(client_bp)

# extensions
# Ensure email models are imported since it has no blueprint
from app.shared.email.models import *  # noqa
register_extensions(app)


# logging
configure_logging(Config.LOG_LEVEL, app=app)
