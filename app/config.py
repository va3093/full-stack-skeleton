""" Global Flask Application Settings """

import os
from distutils.util import strtobool


class Config(object):
    DEBUG = False
    TESTING = False
    BASE_DIR = os.path.dirname(__file__)
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    CLIENT_DIR = os.path.join(BASE_DIR, 'client', 'vue_app')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DB_DB = os.environ.get('DB_DB', 'app')
    DB_PASSWORD = os.environ.get('DB_PASSWORD', 'password')
    DB_USER = os.environ.get('DB_USER', 'app')
    DB_HOST = os.environ.get('DB_HOST', 'localhost')
    SQLALCHEMY_DATABASE_URI = (
        f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_DB}')
    SECRET_KEY = os.environ.get('SECRET_KEY', 'UnsafeSecret')
    ALLOW_ALL_CORS = strtobool(os.environ.get('ALLOW_ALL_CORS', 'False'))
    LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
    MEOS_SENTRY_DSN = os.environ.get('MEOS_SENTRY_DSN', '')

    if not os.path.exists(CLIENT_DIR):
        raise Exception(
            'Client App directory not found: {}'.format(CLIENT_DIR))
