import logging

from celery import Celery
from flask import Flask

from app.config import Config
from app.extensions import register_extensions


_logger = logging.getLogger(__name__)

broker = 'sqla+' + Config.SQLALCHEMY_DATABASE_URI

celery = Celery(broker=broker)

application = Flask(__name__)
application.config.from_object(Config)
register_extensions(application, exclude_metrics=True)


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        10,
        example.s(),
        name='Example')


@celery.task
def example():
    pass
