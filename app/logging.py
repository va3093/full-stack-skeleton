import requests  # noqa
import http.client
import logging
import json_logging

_logger = logging.getLogger(__name__)


def configure_logging(log_level, *, app):
    # requests
    http.client.HTTPConnection.debuglevel = 1

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

    # general
    logging.basicConfig()
    root_logger = logging.getLogger()
    root_logger.setLevel(log_level)
    root_logger.addHandler(logging.StreamHandler())
    json_logging.ENABLE_JSON_LOGGING = True
    json_logging.init(framework_name='flask')
    # json_logging.init_request_instrument(app)

    # This is to remove annoying google cache error
    logging.getLogger(
        'googleapiclient.discovery_cache').setLevel(logging.ERROR)


def log_event(*, event_key, user_id=None):
    _logger.info("Event", extra={'props': {
        'event_key': event_key,
        'user_id': user_id
    }})
