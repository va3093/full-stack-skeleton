from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from prometheus_flask_exporter import PrometheusMetrics
from raven.contrib.flask import Sentry
from app.config import Config


db = SQLAlchemy()
migrate = Migrate()
metrics = None


def register_extensions(app, exclude_metrics=False):
    """Register Flask extensions."""
    # ensure models are registered
    from app.api import models  # noqa
    from app.shared.email import models  # noqa

    global metrics

    db.init_app(app)
    migrate.init_app(app, db)
    Sentry(app, dsn=Config.MEOS_SENTRY_DSN)
    if not exclude_metrics:
        metrics = PrometheusMetrics(app, path='/meos_app_metrics')
