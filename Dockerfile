# client building
FROM node:9.11.1-alpine as build-stage
COPY . /client_app
WORKDIR /client_app/app/client/vue_app

RUN npm install
RUN npm run build

# Python app
FROM python:3-alpine

# Required dependencies for uwsgi and sqlalchemy
RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev \
  && pip install psycopg2 \
  && apk del build-deps \
  && apk add gcc \
  && apk add libc-dev \
  && apk add linux-headers

COPY . /meos_website
COPY --from=build-stage /client_app/app/client/vue_app/dist /meos_website/app/client/vue_app/dist

WORKDIR /meos_website

RUN pip install -r requirements.txt

CMD ["./run_api.sh"]

