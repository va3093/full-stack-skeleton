# Full stack skeleton

## Features
* Minimal Flask App with modular Config
* [vue-cli 3](https://github.com/vuejs/vue-cli/blob/dev/docs/README.md) with Babel and ESlint.
* [Vuex](https://vuex.vuejs.org/) for state management
* [Vue Router](https://router.vuejs.org/)
* [Axios](https://vuex.vuejs.org/) for backend communication
* Sample Vue [Filters](https://vuejs.org/v2/guide/filters.html)
* Heroku Configuration with one-click deployment

## Template Structure

The api has a decoupled client server architecture served by the same webserver. Flask blueprints are used to achieve this.

There is special blueprint with the path `/client` that will return the Vue app. All pages within the Vue app are appended to the `/client` path. Eg. `/client/login`.


## Installation

1) Creat a Gitlab project for your new app

2) Clone the skeleton repo
```
git clone git@gitlab.com:va3093/full-stack-skeleton.git
```

3) Rename the repo and change the origin to point your blank repo
```
mv full-stack-skeleton your_app_name
cd your_app_name
git remote remove origin
git remote add origin git@gitlab.com:user_name/your_app_name.git
```

4) Install python dendencies
```
mkvirtualenv your_app_name
pip install -r test_requirements.txt
```

5) Install npm dependencies
```
make npm_install
```

6) Create a CI variable `GITLAB_REGISTRY_TOKEN` whose value is your personal access token so that you your app's docker image will be stored in the gitlab registry that is accessible.

7) Update `.gitlab-ci.yml` and find and replace `full-stack-skeleton` with `YOUR_APP_NAME`.

8) Run Postgres
```
make run_postgres
```

9) Run migrations
```
make db_migrate
make db_upgrade
```

10) Build vue app
```
make build_client_dev
```

11) Check everything is working
```
make run_debug
```
You should see the vue app showing results of a call that retrieves some data from the database

##### Api Server

From the root directory run:

```
$ python -m app serve_api
```

This will start the flask development server on `localhost:5000` and will respond to all requests on `/api.`.
This command is the same as running `python run.py`

##### Client Server

Start another terminal window, and from the same directory run:

```
$ python -m app serve_client
```

This will launch your browser and server the Vue application on `localhost:8080`. T
he vue app will hit `localhost:5000` to fetch resources.

This combination allows you have both your backend python files, as well as the Vue app files autoreload on each file save.


## Production Server

The production server uses Gunicorn to serve the entire application.
This template is configured to work with Heroku out of the box - just make sure you run `npm run build` before pushing it to your Heroku repository.

* Build your Vue Application:
```
$ python -m app build
```
This commands is a shorcut for cd-ing into `/app/client/vue_app` and running `$ npm run build`.

* Commit your code

* Setup your heroku app:

	```
	$ heroku apps:create flask-vuejs-template
	$ heroku config:set FLASK_CONFIG=Production
	$ heroku config:set SECRET=SECRETKEY
	$ heroku git:remote --app flask-vuejs-template
	```
* Push your application to heroku:

	```$ git push heroku```

### Heroku deployment - One Click Deploy

You can spin up your on version of this template on Heroku:

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/gtalarico/flask-vuejs-template)
