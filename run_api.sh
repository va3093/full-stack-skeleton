#!/bin/sh

flask db upgrade

if [ "$RUN_TYPE" = "CELERY_WORKER" ]; then
    rm -f /tmp/*.pid
    celery -A app.tasks worker --loglevel=debug
elif [ "$RUN_TYPE" = "CELERY_BEAT" ]; then
    rm -f celerybeat.pid
    celery -A app.tasks beat --loglevel=debug
else
    uwsgi --ini uwsgi.ini
fi
