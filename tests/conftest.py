import logging
import os

import pytest
from sqlalchemy_utils import (
    create_database, database_exists, drop_database)

from app import app as application
from app.config import Config
from app.shared.database import db as _db

logging.getLogger('googleapiclient.discovery').setLevel(logging.ERROR)


class TestConfig(Config):
    DB_DB = os.environ.get('DB_DB', 'test')
    DB_PASSWORD = os.environ.get('DB_PASSWORD', 'password')
    DB_USER = os.environ.get('DB_USER', 'meos_website')
    DB_HOST = os.environ.get('DB_HOST', 'localhost')
    SQLALCHEMY_DATABASE_URI = (
        f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_DB}')


@pytest.fixture(scope="module")
def app():
    application.config.from_object(TestConfig)
    return application


@pytest.fixture(scope="module")
def client(app):
    app.config.from_object(TestConfig)
    with app.test_client() as client:
        app.app_context().push()
        yield client


@pytest.fixture(scope="module")
def request_context():
    return app.test_request_context('')



@pytest.fixture
def db(app):
    """A database for the tests."""
    if not database_exists(TestConfig.SQLALCHEMY_DATABASE_URI):
        create_database(TestConfig.SQLALCHEMY_DATABASE_URI)
    _db.app = app
    _db.drop_all()
    with app.app_context():
        _db.create_all()

    yield _db

    # Explicitly close DB connection
    _db.session.execute("DROP TABLE IF EXISTS alembic_version;")
    _db.session.commit()
    _db.session.close()
    _db.drop_all()
